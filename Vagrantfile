# -*- mode: ruby -*-
# vi: set ft=ruby :

# Every Vagrant development environment requires a box. You can search for
# boxes at https://atlas.hashicorp.com/search.
BOX_IMAGE = "generic/debian10"
NODE_COUNT = 2

Vagrant.configure("2") do |config|
  config.ssh.forward_agent = true

  unless Vagrant.has_plugin?("vagrant-vbguest")
    puts 'Installing vagrant-vbguest Plugin...'
    system('vagrant plugin install vagrant-vbguest')
  end

  config.vm.define "master" do |subconfig|
    subconfig.vm.box = BOX_IMAGE
    subconfig.vm.hostname = "master"
    subconfig.vm.network :private_network, ip: "10.0.0.10"

    subconfig.vm.synced_folder ".", '/vagrant', disabled:true
    subconfig.vm.synced_folder "./iac", '/usr/iac'
  end

  (1..NODE_COUNT).each do |i|
    config.vm.define "node#{i}" do |subconfig|
      subconfig.vm.box = BOX_IMAGE
      subconfig.vm.hostname = "node#{i}"
      subconfig.vm.network :private_network, ip: "10.0.0.#{i + 10}"
    end
  end

  # Install avahi on all machines
  config.vm.provision "shell", inline: <<-SHELL
    apt install -y avahi-daemon libnss-mdns
  SHELL

  # Authorize host machine ssh key on virtual machines.
  config.vm.provision "shell" do |s|
    ssh_pub_key = File.readlines("#{Dir.home}/.ssh/id_rsa.pub").first.strip
    s.inline = <<-SHELL
      echo #{ssh_pub_key} >> /home/vagrant/.ssh/authorized_keys
    SHELL
  end

  # Install ansible  
  config.vm.provision "shell", inline: <<-SHELL
    apt update
    apt install python3-pip
    python3 -m pip install --user ansible
  SHELL

  # Install latest Python3
  # use nproc to discover total cpu cores for the make command
#  config.vm.provision "shell", inline: <<-SHELL
#    apt update
#    apt install -y build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libsqlite3-dev libreadline-dev libffi-dev curl libbz2-dev
#    wget https://www.python.org/ftp/python/3.9.16/Python-3.9.16.tar.xz
#    tar -xf Python-3.9.16.tar.xz
#    sh -c "cd Python-3.9.16 && ./configure --enable-optimizations"
#    sh -c "cd Python-3.9.16 && make -j 2"
#    sh -c "cd Python-3.9.16 && make altinstall"
#    sh -c "rm /usr/bin/python3 && ln -sf /usr/local/bin/python3.9 /usr/bin/python3"
#  SHELL

end
